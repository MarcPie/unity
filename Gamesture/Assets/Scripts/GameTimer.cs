﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : MonoBehaviour {
	private IList<TimerDelay> listDelays = new List<TimerDelay>();

	public void start(float delay_in_seconds, Action on_elapsed) {
		listDelays.Add(new TimerDelay(delay_in_seconds, on_elapsed));
	}
	public void abort(Action on_elapsed) {
		for (int i = listDelays.Count - 1; i >= 0 ; i--) {
            TimerDelay delay = listDelays[i];
            if( delay.action == on_elapsed ) {
                listDelays.RemoveAt(i);
            }
		}
	}
    public bool isActive(Action on_elapsed) {
        foreach( var delay in listDelays ) {
            if( delay.action == on_elapsed ) {
                return true;
            }
		}
        return false;
    }

	void Update() {
		for (int i = listDelays.Count - 1; i >= 0 ; i--) {
            TimerDelay delay = listDelays[i];
			delay.time -= Time.deltaTime;
			if( delay.time <= 0.0f ) {
				listDelays.RemoveAt(i);
				delay.action();
			}
		}
	}

	private class TimerDelay {
		public float time;
		public Action action;
		public TimerDelay(float delay_time, Action delay_action) {
			time = delay_time;
			action = delay_action;
		}
	}
}
