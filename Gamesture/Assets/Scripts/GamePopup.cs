﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePopup : MonoBehaviour {
    public delegate void PopupEvent(GamePopup target);

    public Button ButtonConfirm;

	void Start() {
        ButtonConfirm.onClick.AddListener(onConfirm);
	}

    public event PopupEvent eventConfirm;

	private void onConfirm() {
        if( eventConfirm != null ) eventConfirm(this);
    }
}
