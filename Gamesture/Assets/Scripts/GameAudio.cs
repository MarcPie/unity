﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAudio : MonoBehaviour {
	public AudioSource[] AudioSounds;
	public void play() {
        if( AudioSounds.Length == 0 ) Debug.LogWarning("No sounds were provided to "+typeof(GameAudio).Name);
        else {
            AudioSource source = AudioSounds[Random.Range(0, AudioSounds.Length)];
            source.PlayOneShot(source.clip);
        }
    }
}
