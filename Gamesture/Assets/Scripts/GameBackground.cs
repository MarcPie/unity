﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class GameBackground : MonoBehaviour {
    [Range(0.1f, 2.0f)] public float MovementSpeed = 1;
	private RectTransform[] arrClouds;
    private uint nLength;
	void Start() {
		nLength = (uint)transform.childCount;
		arrClouds = new RectTransform[nLength];
		for( int i = 0; i < nLength; i++ ) {
            arrClouds[i] = (RectTransform)transform.GetChild(i);
		}
	}

	void Update() {
        for( int i = 0; i < nLength; i++ ) {
            RectTransform cloud = arrClouds[i];
            cloud.Translate(MovementSpeed, 0, 0, Space.Self);
            if( cloud.localPosition.x > Screen.width ) {
                int posx = (int)( (cloud.rect.width * cloud.localScale.x) + Random.Range(0,10) );
                int posy = (int)(Random.Range(0, 100)*cloud.rect.y);
                cloud.localPosition = new Vector3(-posx, posy, 0);
            }
        }
	}
}
