﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class GameScreen : MonoBehaviour {
	public GamePopup GamePopup;
	public GameAudio GameAudio;
	public GameTimer GameTimer;

	public Button ButtonPopup;
	public Button ButtonSound;
	public Button ButtonText;

	public GameObject ObjectMainText;
	public GameObject ObjectInfoText;

	private CanvasGroup uiGroup;
	private bool bInfo;

	void Start() {
		uiGroup = GetComponent<CanvasGroup>();
		showInfo(false);
		showPopup(false);
		ButtonPopup.onClick.AddListener( onPopup );
		ButtonSound.onClick.AddListener( onSound );
		ButtonText.onClick.AddListener( onText );
		GamePopup.eventConfirm += onPopupConfirm;
	}

	public void showInfo(bool is_showing) {
		bInfo = is_showing;
		ObjectMainText.SetActive(!bInfo);
		ObjectInfoText.SetActive(bInfo);
	}

	public void showPopup(bool is_visible) {
		GamePopup.gameObject.SetActive(is_visible);
		ButtonPopup.enabled = !is_visible;
		ButtonSound.enabled = !is_visible;
		ButtonText.enabled = !is_visible;
		uiGroup.alpha = is_visible ? 0.5f : 1.0f;
	}

	private void onPopup() {
		showPopup(true);
	}

	private void onSound() {
		GameAudio.play();
	}

	private void onText() {
		if( ObjectInfoText.activeSelf ) return;
		GameTimer.start(3.0f, onTextEnd);
		showInfo(true);
	}
	private void onTextEnd() {
		showInfo(false);
	}

	private void onPopupConfirm(GamePopup popup) {
		showPopup(false);
	}
}
