﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MazeGen {
	public class MazeTrail {
		public readonly uint index;

        public Vector2Int node;
		public MazeTrail parent;
		public MazePath owner;

		public MazeTrail(uint idx) {
			index = idx;
		}

	}
}