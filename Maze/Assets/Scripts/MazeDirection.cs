namespace MazeGen {
	public readonly struct MazeDirection {
		public static readonly MazeDirection LEFT = new MazeDirection(-1,0, MazeDirectionType.LEFT, MazeDirectionType.RIGHT);
		public static readonly MazeDirection RIGHT = new MazeDirection(1,0, MazeDirectionType.RIGHT, MazeDirectionType.LEFT);
		public static readonly MazeDirection UP = new MazeDirection(0,-1, MazeDirectionType.UP, MazeDirectionType.DOWN);
		public static readonly MazeDirection DOWN = new MazeDirection(0,1, MazeDirectionType.DOWN, MazeDirectionType.UP);

		public readonly MazeDirectionType type;
		public readonly MazeDirectionType opposite;
		public readonly sbyte x;
		public readonly sbyte y;
		public MazeDirection(sbyte dir_x, sbyte dir_y, MazeDirectionType dir_type, MazeDirectionType dir_opposite) {
			x = dir_x;
			y = dir_y;
			type = dir_type;
			opposite = dir_opposite;
		}
	}

	public enum MazeDirectionType {
		NONE = 0x0000,
		UP = 0x000F,
		RIGHT = 0x00F0,
		DOWN = 0x0F00,
		LEFT = 0xF000
	}
}