using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MazeGen {
	public class MazeGenerator {
		private MazeMap mazeMap = new MazeMap(0,0);
		private Queue<MazePath> queuePaths = new Queue<MazePath>(1024);
		private IList<MazePosition> listEntry = new List<MazePosition>();
		private IList<MazePosition> listExit = new List<MazePosition>();
		private bool[,] arrResult = new bool[0,0];
		private byte nBorder = 0;
		private uint nWidth = 0;
		private uint nHeight = 0;

		public bool[,] map => arrResult;

		public void generate(uint maze_width, uint maze_height, bool include_border = true, bool start_on_edge = true) {
			nWidth = maze_width;
			nHeight = maze_height;
			if( include_border ) {
				if( nWidth <= 2 || nHeight <= 2 ) {
					listEntry.Clear();
					mazeMap = new MazeMap(0, 0);
					arrResult = new bool[0, 0];
					return;
				}
				nBorder = 1;
				mazeMap = new MazeMap((uint)Mathf.CeilToInt((nWidth - (nBorder * 2)) / 2f), (uint)Mathf.CeilToInt((nHeight - (nBorder * 2)) / 2f));
			}
			else {
				nBorder = 0;
				mazeMap = new MazeMap((uint)Mathf.CeilToInt(nWidth / 2f), (uint)Mathf.CeilToInt(nHeight / 2f));
			}

			arrResult = new bool[nWidth, nHeight];

			MazeNode node_start = mazeMap.drawStart(start_on_edge);
			MazePath path_start = new MazePath(node_start);
			queuePaths.Enqueue(path_start);

			while( queuePaths.Count > 0 ) {
				MazePath current = queuePaths.Dequeue();
				MazePathingResult traveled = current.travel(mazeMap);
				if( traveled == MazePathingResult.PROGRESS ) {
					queuePaths.Enqueue(new MazePath(current.activeNode));
					queuePaths.Enqueue(current);
				}
				else if( traveled == MazePathingResult.YIELD ) {
					queuePaths.Enqueue(current);
				}
			}

			//since MazeMap is generated using half of the resolution, actual maze needs to be "translated" it into real x/y
			for( uint y = 0, ry = nBorder; y < mazeMap.height; y++, ry += 2 ) { //note the two variables 
				for( uint x = 0, rx = nBorder; x < mazeMap.width; x++, rx += 2 ) {//also note the flipped loop order (y/x)
					var node = mazeMap.get(x,y);
					if( node == null ) continue;
					arrResult[rx, ry] = true;
					if( node.hasConnectionRight ) arrResult[rx + 1, ry] = true;
					if( node.hasConnectionDown ) arrResult[rx, ry + 1] = true;
				}
			}

			//carve a path for the entry point
			listEntry.Clear();
			if( start_on_edge ) {
				carveEscape(node_start, ref listEntry);
			}
		}

		public IList<MazePosition> solve(bool exit_on_edge = true, ushort preferrable_length = ushort.MaxValue) {
			MazeNode node_exit = null;
			int lowest_dist = preferrable_length;
			for( uint i = 0; i < mazeMap.width; i++ ) {
				for( uint j = 0; j < mazeMap.height; j++ ) {
					var node = mazeMap.get(i, j);
					if( node == null ) continue;
					if( (exit_on_edge &&
						node.position.x != 0 && node.position.x != mazeMap.xMax &&
						node.position.y != 0 && node.position.y != mazeMap.yMax) || 
						(!exit_on_edge && !node.hasSingleConnection) ) continue;

					var dist = System.Math.Abs(preferrable_length - node.step);
					if( dist < lowest_dist ) {
						lowest_dist = dist;
						node_exit = node;
					}
				}
			}
			if( node_exit == null ) return new List<MazePosition>();

			listExit.Clear();
			if( exit_on_edge ) {
				carveEscape(node_exit, ref listExit);
			}

			// note that solution is crafted starting from the end, and then reversed when completed
			List<MazePosition> solution = new List<MazePosition>(listEntry.Count + node_exit.step + listExit.Count);
			for( int i = 0; i < listExit.Count; i++ ) {
				solution.Add(listExit[i]);
			}
			int step = int.MaxValue;
			while( node_exit != null && step > 0 ) {
				uint rx = nBorder + (node_exit.position.x*2);
				uint ry = nBorder + (node_exit.position.y*2);
				solution.Add( new MazePosition(rx, ry) );
				switch(node_exit.previousDirection) {
					case MazeDirectionType.LEFT: solution.Add( new MazePosition(rx-1, ry) ); break;
					case MazeDirectionType.RIGHT: solution.Add( new MazePosition(rx+1, ry) ); break;
					case MazeDirectionType.UP: solution.Add( new MazePosition(rx, ry-1) ); break;
					case MazeDirectionType.DOWN: solution.Add( new MazePosition(rx, ry+1) ); break;
				}
				node_exit = node_exit.previous;
				step --;
			}
			for( int i = listEntry.Count - 1; i >= 0; i-- ) {
				solution.Add(listEntry[i]);
			}
			solution.Reverse();

			return solution;
		}

		private void carveEscape(MazeNode from, ref IList<MazePosition> edited_positions) {
			MazePosition start = from.position;
			uint inner_x = (start.x*2) + nBorder;
			uint inner_y = (start.y*2) + nBorder;
			uint entry_x, entry_y;
			if( start.x == mazeMap.xMax || start.x == 0 ) {
				entry_x = (start.x / mazeMap.xMax) * (nWidth - 1);
				entry_y = inner_y;
			}
			else {
				entry_x = inner_x;
				entry_y = (start.y / mazeMap.yMax) * (nHeight - 1);
			}
			int dir_x = System.Math.Sign((int)(inner_x - entry_x));
			int dir_y = System.Math.Sign((int)(inner_y - entry_y));
			while( entry_x != inner_x || entry_y != inner_y ) {
				arrResult[entry_x, entry_y] = true;
				edited_positions.Add(new MazePosition(entry_x, entry_y));
				entry_x = (uint)(entry_x + dir_x);
				entry_y = (uint)(entry_y + dir_y);
			}
		}
	}
}