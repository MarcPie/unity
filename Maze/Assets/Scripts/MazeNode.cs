namespace MazeGen {
	public class MazeNode {
		public readonly MazePosition position;
		public readonly MazeNode previous;
		public readonly MazeDirectionType previousDirection;
		public uint connections;
		public ushort step;
		public MazeNode(MazePosition node_position, MazeNode node_previous = null, MazeDirectionType node_previous_direction = MazeDirectionType.NONE) {
			position = node_position;
			previous = node_previous;
			previousDirection = node_previous_direction;
		}
		public void setConnection(MazeDirectionType type) {
			connections = connections|(uint)type;
		}
		public bool hasConnection(MazeDirectionType type) {
			return (connections&(uint)type)>0;
		}

		public bool hasSingleConnection => 
			connections != 0 && (
			(connections^(uint)MazeDirectionType.LEFT) == 0 ||
			(connections^(uint)MazeDirectionType.DOWN) == 0 ||
			(connections^(uint)MazeDirectionType.UP) == 0 ||
			(connections^(uint)MazeDirectionType.RIGHT) == 0);

		public bool hasConnectionLeft => (connections&(uint)MazeDirectionType.LEFT)>0;
		public bool hasConnectionRight => (connections&(uint)MazeDirectionType.RIGHT)>0;
		public bool hasConnectionUp => (connections&(uint)MazeDirectionType.UP)>0;
		public bool hasConnectionDown => (connections&(uint)MazeDirectionType.DOWN)>0;

	}

	public enum MazeNodeType {
		UNKNOWN,
		SIDE_PATH,
		MAIN_PATH
	}
}