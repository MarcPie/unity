namespace MazeGen {
	public static class MazeRandomizer {
		private static float[] values = new float[255];
		private static int step = 0;
		static MazeRandomizer() {
			for( int i = values.Length - 1; i >= 0; i-- ) {
				values[i] = (float)i / 255.0f;
			}
			for( int i = 0; i < values.Length - 1; i++ ) { //length-1 since all that shuffling requires at least two elements
				int idx = UnityEngine.Random.Range(i+1, values.Length);
				float tmp = values[i];
				values[i] = values[idx];
				values[idx] = tmp;
			}
		}
		///<summary>Returns a number between 0.0 and 1.0 (including 0.0, but excluding 1.0).</summary>
		public static float get() {
			return values[step = (step + 1) % values.Length];
		}
	}
}