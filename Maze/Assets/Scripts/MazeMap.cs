using UnityEngine;
namespace MazeGen {
	public class MazeMap {
		public readonly uint width;
		public readonly uint height;
		public readonly uint xMax;
		public readonly uint yMax;
		private MazeNode[,] arrNodes;
		public MazeMap(uint maze_width, uint maze_height) {
			width = maze_width;
			height = maze_height;
			xMax = maze_width-1;
			yMax = maze_height-1;
			arrNodes = new MazeNode[width, height];
		}

		public bool contains(MazePosition pos) => pos.x < width && pos.y < height;
		public MazeNode get(MazePosition pos) => arrNodes[pos.x, pos.y];
		public MazeNode get(uint x, uint y) => arrNodes[x,y];
		public MazeNode draw(MazePosition pos, MazeNode previous, MazeDirectionType previous_direction) {
			var node = arrNodes[pos.x, pos.y];
			if( node == null ) {
				node = new MazeNode(pos, previous, previous_direction);
				arrNodes[pos.x, pos.y] = node;
			}
			return node;
		}
		public MazeNode draw(uint x, uint y, MazeNode previous, MazeDirectionType previous_direction) => draw(new MazePosition(x,y), previous, previous_direction);

		public MazeNode drawStart(bool start_on_edge = true) {
			MazePosition pos;
			if( start_on_edge ) {
				int extremum = Mathf.RoundToInt(Random.value); // either min or max
				if( Mathf.RoundToInt(Random.value) > 0.5 ) { //either x or y edge
					pos = new MazePosition( (uint)(extremum*xMax), (uint)(Random.value * yMax) );
				}
				else {
					pos = new MazePosition( (uint)(Random.value * xMax), (uint)(extremum*yMax) );
				}
			}
			else {
				pos = new MazePosition( (uint)(Random.value * xMax), (uint)(Random.value * yMax) );
			}
			var start = draw(pos, null, MazeDirectionType.NONE);
			start.step = 0;
			return start;
		}
	}
}