namespace MazeGen {
	public class MazeLookup {
		private readonly MazeDirection[] arrLookup = new MazeDirection[]{MazeDirection.LEFT, MazeDirection.RIGHT, MazeDirection.UP, MazeDirection.DOWN};
		private byte nSize;
		public MazeLookup() {
			nSize = (byte)arrLookup.Length;
		}

		public bool canLook => nSize > 0;

		public MazeDirection look() {
			byte idx = (byte)(MazeRandomizer.get()*nSize);
			nSize--;
			if( idx == nSize ) return arrLookup[idx];
			// "used" values are moved outside of the accessable range so they won't be available on the next iteration
			var result = arrLookup[idx];
			var tmp = arrLookup[nSize];
			arrLookup[idx] = tmp;
			arrLookup[nSize] = result;
			return result;
		}

		public void reset() {
			nSize = (byte)arrLookup.Length;
		}
	}
}