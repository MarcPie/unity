﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MazeGen;

[ExecuteInEditMode]
public class Maze : MonoBehaviour {
	[SerializeField][HideInInspector] private uint _width = 1;
	[SerializeField][HideInInspector] private uint _height = 1;
	[SerializeField][HideInInspector] private uint _size = 2;
	[SerializeField][HideInInspector] private bool _hasBorders = true;
	[SerializeField][HideInInspector] private MazePathType _pathType = MazePathType.ANY_TO_ANY;
	[SerializeField][HideInInspector] private ushort _length = 64;
	[SerializeField][HideInInspector] private bool _showPath = true;

	private GameObject _objectRoot;
	private Transform _transformRoot;
	private bool _isDirty;

	private Material _materialPath;
	
	private MazeGenerator _mazeGenerator;
	private IList<GameObject> _listPath;

	void Awake() => init();
	void Start() => init();
	void OnFocus() => init();

	private void init() {
		if( _mazeGenerator != null ) return;
		_objectRoot = gameObject;
		_transformRoot = transform;
		_materialPath = new Material(Shader.Find("Diffuse"));
		_materialPath.color = Color.red;
		_mazeGenerator = new MazeGenerator();
	}

	/// <summary>Indicates the maze's width, in an amount of Cubes per row.</summary>
	public uint width {
		get { return _width; }
		set {
			if(_width == value) return;
			_width = value;
			redraw();
		}
	}

	/// <summary>Indicates the maze's height, in an amount of Cubes per column.</summary>
	public uint height {
		get { return _height; }
		set {
			if(_height == value) return;
			_height = value;
			redraw();
		}
	}

	/// <summary>Indicates the size of a single Cube in the maze's structure.</summary>
	public uint size {
		get { return _size; }
		set {
			if(_size == value) return;
			_size = value;
			redraw();
		}
	}

	/// <summary>Whether to include borders around generated maze.</summary>
	public bool includeBorders {
		get { return _hasBorders; }
		set {
			if(_hasBorders == value) return;
			_hasBorders = value;
			redraw();
		}
	}

	/// <summary>Indicates how the path through the maze (solution) will look.</summary>
	public MazePathType pathType {
		get { return _pathType; }
		set {
			if(_pathType == value) return;
			_pathType = value;
			redraw();
		}
	}

	/// <summary>Hints the desired path length through the maze (solution) to the generator.</summary>
	public ushort pathLength {
		get { return _length; }
		set {
			if(_length == value) return;
			_length = value;
			redraw();
		}
	}

	/// <summary>Whether to show the path through the maze (solution) or not.</summary>
	public bool showPath {
		get { return _showPath; }
		set {
			if(_showPath == value) return;
			_showPath = value;
			redrawPath();
		}
	}

	/// <summary>
	/// Update reconstructs the maze if its properties have changed.
	/// </summary>
	void Update() {
		if( !_isDirty ) return;
		generate();
		_isDirty = false;
	}

	/// <summary>
	/// Reconstructs the maze based on <c>width</c> and <c>height</c> properties.
	/// Maze walls are made out Cubes scaled to size indicated by <c>scale</c>.
	/// </summary>
	public void generate() {
		init();

		for( int i = _transformRoot.childCount - 1; i >= 0; i-- ) {
			var child = _transformRoot.GetChild(i);
			child.parent = null;
			UnityEngine.Object.DestroyImmediate(child.gameObject);
		}

		_mazeGenerator.generate(_width, _height, _hasBorders, _pathType == MazePathType.EDGE_TO_ANY || _pathType == MazePathType.EDGE_TO_EDGE);
		IList<MazePosition> path = _mazeGenerator.solve(_pathType == MazePathType.EDGE_TO_EDGE, _length);
		bool[,] map = _mazeGenerator.map; 
		for (int y = map.GetLength(1) - 1; y >= 0 ; y--) {
			for (int x = map.GetLength(0) - 1; x >= 0 ; x--) {
				var tile = map[x, y];
				if( !tile ) {
					var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
					cube.transform.localScale = new Vector3(_size, _size, _size);
					cube.transform.localPosition = new Vector3(x * _size, 0, y * _size);
					cube.transform.SetParent(_transformRoot, false);
				}
			}
		}

		_listPath = new List<GameObject>(path.Count);
		foreach(var position in path) {
			var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.GetComponent<MeshRenderer>().material = _materialPath;
			cube.transform.localScale = new Vector3(_size, _size, _size);
			cube.transform.localPosition = new Vector3(position.x * _size, 0, position.y * _size);
			cube.transform.SetParent(_transformRoot, false);
			_listPath.Add(cube);
		}
		redrawPath();
	}

	/// <summary>
	/// Redraws the path through the maze (solution).
	/// Depending on the state of <c>showPath</c> it will either show the full solution or just the start and end markers.
	/// </summary>
	private void redrawPath() {
		if( _showPath) foreach(var path in _listPath) {
			path.SetActive(true);
		}
		else {
			foreach(var path in _listPath) {
				path.SetActive(false);
			}
			if( _listPath.Count > 1 ) {
				_listPath[0].SetActive(true);
				_listPath[_listPath.Count-1].SetActive(true);
			}
		}
	}

	/// <summary>
	/// Marks the maze for redraw. It will be reconstructed in the next <c>Update</c>.
	/// </summary>
	private void redraw() {
		_isDirty = true;
	}
}


[CustomEditor(typeof(Maze))]
public class MazeEditor : Editor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		Maze maze = target as Maze;
		//Max() is used to clamp values (can't accept negative stuff since these are UINT fields)
		maze.width = (uint)Math.Max(EditorGUILayout.IntField("Width:", Math.Max((int)maze.width,0), GUILayout.MinWidth(100)),0);
		maze.height = (uint)Math.Max(EditorGUILayout.IntField("Height:", Math.Max((int)maze.height,0), GUILayout.MinWidth(100)),0);
		maze.size = (uint)Math.Max(EditorGUILayout.IntField("Cube size:", Math.Max((int)maze.size,0), GUILayout.MinWidth(100)),0);
		maze.includeBorders = EditorGUILayout.Toggle("Include borders: ", maze.includeBorders);
		maze.pathType = (MazePathType)EditorGUILayout.EnumPopup("Path: ", maze.pathType);
		maze.pathLength = (ushort)Math.Max(EditorGUILayout.IntField("Path length:", Math.Max((int)maze.pathLength,0), GUILayout.MinWidth(100)),0);
		maze.showPath = EditorGUILayout.Toggle("Display solution: ", maze.showPath);

		if( GUILayout.Button("Re-generate") ) {
			maze.generate();
		}

		if( GUI.changed ) {
			EditorUtility.SetDirty(target);
		}
	}
}

public enum MazePathType {
	ANY_TO_ANY,
	EDGE_TO_ANY,
	EDGE_TO_EDGE
}