
namespace MazeGen {
	public class MazePath {
		private readonly MazeLookup mazeLookup = new MazeLookup();
		private MazeNode mazeNode;
		public MazePath(MazeNode path_start) {
			mazeNode = path_start;
		}
		public MazeNode activeNode => mazeNode;
		public MazePathingResult travel(MazeMap map) {
			if( !mazeLookup.canLook ) return MazePathingResult.END;

			MazeDirection dir = mazeLookup.look();
			if( !mazeNode.position.canMove(dir) ) return MazePathingResult.YIELD;

			MazePosition move = mazeNode.position.move(dir);
			if( !map.contains(move) ) return MazePathingResult.YIELD;

			MazeNode target = map.draw(move, mazeNode, dir.opposite);
			if( target.connections != 0 ) return MazePathingResult.YIELD;

			mazeLookup.reset();

			target.step = (ushort)(mazeNode.step+1);
			target.setConnection(dir.opposite);
			mazeNode.setConnection(dir.type);
			mazeNode = target;
			return MazePathingResult.PROGRESS;
		}
	}
	
	public enum MazePathingResult {
		END,
		YIELD,
		PROGRESS
	}
}