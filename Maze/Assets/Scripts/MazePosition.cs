namespace MazeGen {
	public readonly struct MazePosition {
		public readonly uint x;
		public readonly uint y;
		public MazePosition(uint pos_x, uint pos_y) {
			x = pos_x;
			y = pos_y;
		}
		public bool canMove(MazeDirection dir) {
			return (dir.x < 0 && x > 0) ||
				(dir.y < 0 && y > 0) ||
				(dir.x > 0 && x < uint.MaxValue) ||
				(dir.y > 0 && y < uint.MaxValue);
		}
		public MazePosition move(MazeDirection dir) {
			return new MazePosition((uint)(x+dir.x), (uint)(y+dir.y));
		}
	}
}