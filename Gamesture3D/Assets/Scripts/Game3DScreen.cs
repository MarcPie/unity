﻿using System.Collections;
using UnityEngine;
using System;

public class Game3DScreen : MonoBehaviour {
	public Game3DPopup GamePopup;
	public Game3DAudio GameAudio;

	public Game3DButton GameButtonPopup;
	public Game3DButton GameButtonSound;
	public Game3DButton GameButtonText;

	public GameObject ObjectMainText;
	public GameObject ObjectInfoText;

	private bool bInfo;

	void Start() {
		showInfo(false);
		showPopup(false);
		GameButtonPopup.eventMouseUp += onPopup;
		GameButtonSound.eventMouseUp += onSound;
		GameButtonText.eventMouseUp += onText;
		GamePopup.eventConfirm += onPopupConfirm;
	}

	public void showInfo(bool is_showing) {
		bInfo = is_showing;
		ObjectMainText.SetActive(!bInfo);
		ObjectInfoText.SetActive(bInfo);
	}

	public void showPopup(bool is_visible) {
		GamePopup.gameObject.SetActive(is_visible);
		GameButtonPopup.allowInteraction = !is_visible;
		GameButtonSound.allowInteraction = !is_visible;
		GameButtonText.allowInteraction = !is_visible;
	}

	private void onPopup(Game3DButton button) {
		showPopup(true);
	}

	private void onSound(Game3DButton button) {
		GameAudio.play();
	}

	private void onText(Game3DButton button) {
		if( ObjectInfoText.activeSelf ) return;
		StartCoroutine( onDelay(3f, onTextEnd) );
		showInfo(true);
	}
	private void onTextEnd() {
		showInfo(false);
	}

	private void onPopupConfirm(Game3DPopup popup) {
		showPopup(false);
	}

	public IEnumerator onDelay(float delay, Action action) {
		if( action != null ) {
			yield return new WaitForSeconds(delay);
			action();
		}
	}
}
