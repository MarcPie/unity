﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game3DButton : MonoBehaviour {

    public delegate void EventMouse(Game3DButton target);

	public GameObject ObjectShape;
	public GameObject ObjectText;
    public Material MaterialDefault;
    public Material MaterialOver;
    public Material MaterialPressed;

	private BoxCollider unityCollider;
    private MeshRenderer unityMesh;
    private TextMesh unityText;

    private bool bPressed;
    private bool bOver;
    private bool bEnabled = true;

	void Start() {
        unityCollider = GetComponent<BoxCollider>();
        unityMesh = ObjectShape.GetComponent<MeshRenderer>();
        unityText = ObjectText.GetComponent<TextMesh>();
	}

    public bool allowInteraction {
        get { return bEnabled; }
        set { 
            if( bEnabled == value ) return;
            enabled = value;
            bEnabled = value;
            unityCollider.enabled = value;
            if( !bEnabled ) {
                bOver = false;
                bPressed = false;
                unityMesh.material = MaterialDefault;
            }
        }
    }

    public string text { 
        get{ return unityText.text; }
        set{ unityText.text = value; }
    }

    void OnMouseOver() {
        if( bOver ) return;
        bOver = true;
        if( bPressed ) {
            unityMesh.material = MaterialPressed;
        }
        else {
            unityMesh.material = MaterialOver;
        }
        if( eventMouseOver != null ) eventMouseOver(this);
    }
    void OnMouseExit() {
        bOver = false;
        unityMesh.material = MaterialDefault;
        if( eventMouseOut != null ) eventMouseOut(this);
    }
    void OnMouseDown() {
        bPressed = true;
        unityMesh.material = MaterialPressed;
    }
    void OnMouseUp() {
        bPressed = false;
        if( !bOver ) return;

        unityMesh.material = MaterialOver;
        if( eventMouseUp != null ) eventMouseUp(this);
    }

    public event EventMouse eventMouseUp;
    public event EventMouse eventMouseOver;
    public event EventMouse eventMouseOut;
}
