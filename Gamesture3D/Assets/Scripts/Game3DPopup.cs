﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game3DPopup : MonoBehaviour {
	public delegate void PopupEvent(Game3DPopup target);

    public Game3DButton GameConfirm;

	void Start() {
        GameConfirm.eventMouseUp += onConfirm;
	}

    public event PopupEvent eventConfirm;

	private void onConfirm(Game3DButton button) {
        if( eventConfirm != null ) eventConfirm(this);
    }
}
